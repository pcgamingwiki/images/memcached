DIR:=$(strip $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST)))))
RUNNER:=docker
IMAGE=memcached
IMAGE_TAG=latest

container:
	$(RUNNER) run --rm -v $(DIR):/build nixos/nix /build/build/container.sh /build/ > image.tar
