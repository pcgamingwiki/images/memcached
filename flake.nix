{
  description = "Memcached docker container";

  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs =
  { self
  , nixpkgs
  , flake-utils
  }:
  let
    SYSTEMS = [
        "x86_64-linux"
        "aarch64-linux"
    ];
  in
  flake-utils.lib.eachSystem SYSTEMS (system:
  let
    pkgs = import nixpkgs {
      inherit system;
    };

    buildContainer = pkgs.dockerTools.streamLayeredImage {
        name = "memcached";
        tag = "latest";
        config = {
            Entrypoint = ["${pkgs.memcached}/bin/memcached"];
            User = "1000";
        };
    };
  in
  rec {
    defaultPackage = packages.containerImage;
    packages = {
        containerImage = buildContainer;
    };
  }
  );
}
