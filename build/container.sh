#!/bin/sh

builddir=$1
set -x
cd $1
nix --extra-experimental-features nix-command --extra-experimental-features flakes build --no-link
$(nix --extra-experimental-features nix-command --extra-experimental-features flakes path-info)